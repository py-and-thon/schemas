## GET /api/search

query-параметры:
- q=поиск
- vk_user_id=ид_вк
- sirius_user_id=ид_сириуса

```
{
    "results": Array<Event>
}
```

Event:
```
{
    "name": string,
    "description": string,
    "city": string,
    "image_url": string,
    "url": string,
    "category": string
}
```


## GET /api/tips

query-параметры:
- vk_user_id=ид_вк
- sirius_user_id=ид_сириуса

```
{
    "tips": Array<string>,
}
```


## GET /api/events

query-параметры:
- vk_user_id=ид_вк
- sirius_user_id=ид_сириуса

```
{
    "sections": [
        {
            "name": string,
            "events": Array<Event>
        }
    ]
}
```

Event:
```
{
    "name": string,
    "description": string,
    "city": string,
    "image_url": string,
    "url": string,
    "category": string
}
```
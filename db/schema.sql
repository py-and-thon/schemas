CREATE TABLE IF NOT EXISTS cities (
        id serial NOT NULL PRIMARY KEY,
        name text NOT NULL,
        KEY text NOT NULL UNIQUE
);

CREATE TABLE IF NOT EXISTS categories (
        id serial NOT NULL PRIMARY KEY,
        name text NOT NULL,
        KEY text NOT NULL UNIQUE
);

CREATE TABLE IF NOT EXISTS events (
        id serial NOT NULL PRIMARY KEY,
        name text NOT NULL,
        description text NOT NULL,
        city_id integer NOT NULL REFERENCES cities (id),
        image_url text NOT NULL,
        url text NOT NULL,
        category_id integer NOT NULL REFERENCES categories (id)
);
